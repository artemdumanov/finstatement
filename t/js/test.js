angular.module('ui.bootstrap.demo', ['ngAnimate', 'ui.bootstrap']);
var app = angular.module('ui.bootstrap.demo');
app.controller('formController', function ($scope,  $rootScope) {
    this.tab = 1;
    this.setTab = function (tabId) {
        this.tab = tabId;
    };
    this.isSet = function (tabId) {
        return this.tab === tabId;
    };

    $rootScope.allData = {};
    $rootScope.nodePropertiesData = {};
    $rootScope.snmpPropertiesData = {};
    $rootScope.monitoringData = {};

    var allInfo = {
        "Brand1": {
            "models": {
                "model1": {
                    "oss": [
                        "os1",
                        "os2"
                    ]
                },
                "model2": {
                    "oss": [
                        "os3",
                        "os4"
                    ]
                }
            }
        },
        "Brand2": {
            "models": {
                "model3": {
                    "oss": [
                        "os5",
                        "os6"
                    ]
                }
            }
        }
    };

    var brands = [];
    $scope.brands = brands;

    angular.forEach(allInfo, function (value, key) {
        brands.push(key);
    });

    var modelsValues;
    $scope.brandValue = function (brand) {
        var models = [];

        angular.forEach(allInfo, function (value, key) {
            if (brand === key) {
                modelsValues = value.models;

                angular.forEach(value.models, function (val, ke) {

                    models.push(ke);
                });

            }
        });
        return models;
    };

    $scope.oss = function (model) {
        var oss = [];

        angular.forEach(modelsValues, function (value, key) {
            if (key === model) {
                angular.forEach(value.oss, function (v, k) {
                    oss.push(v);
                });
            }
        });

        return oss;
    };

    $scope.inlineOptions = {
        showWeeks: true
    };

    $scope.dateOptions = {
        maxDate: new Date(2100, 5, 22),
        startingDay: 1
    };

    $scope.format = 'dd.MM.yyyy';

    var popups = [];
    $scope.popups = popups;
    for (var i = 0; i < 7; i++) {
        popups[i] = {
            opened: false
        };
    }

    $scope.openPopup = function (index) {
        var now = new Date();

        if (index == 0) {
            $scope.inlineOptions.minDate = new Date(now);
            $scope.dateOptions.minDate = new Date(now);
            $scope.dateOptions.maxDate = new Date(2100, 5, 22);
        } else if (index == 1) {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            $scope.dateOptions.maxDate = new Date(now.getTime() - 86400000);
        } else {
            $scope.inlineOptions.minDate = new Date(now.getTime() + 86400000);
            $scope.dateOptions.minDate = new Date(now.getTime() + 86400000);
            $scope.dateOptions.maxDate = new Date(2100, 5, 22);

        }

        popups[index].opened = true;
    };


    $scope.print = function () {
        var allData = allData;
        var nodePropertiesData = nodePropertiesData;
        var snmpPropertiesData = snmpPropertiesData;
        var monitoringData = monitoringData;

        console.log($rootScope.nodePropertiesData);
        console.log($rootScope.snmpPropertiesData);
        console.log($rootScope.monitoringData);
        console.log($rootScope.allData);

        $rootScope.allData = $rootScope.nodePropertiesData + $rootScope.snmpPropertiesData + $rootScope.monitoringData;
    };

    $scope.disableAnyKeyPress = function(){
        return false;
    }

});
