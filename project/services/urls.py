from django.conf.urls import url

from services import views

urlpatterns =[
    url(r'^email$', views.email_service)
]