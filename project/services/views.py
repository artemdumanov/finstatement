import json

from django import views
from django.http import Http404
from django.http import HttpResponse
from django.utils.encoding import iri_to_uri, uri_to_iri
from rest_framework.decorators import api_view
from rest_framework.response import Response

from account_management.templatetags.filters import deurlizadot
from models.models import Account


@api_view(['POST'])
def email_service(request):
    json_data = json.loads(request.body.decode('utf-8'))
    # print(json_data)
    email = json_data['email']

    # print('EMAIL SEND : ' + email)
    # print('DEURLIZE EMAIL SEND : ' + deurlizadot(email))

    if Account.objects.get(email=uri_to_iri(email)):
        print("EXIST")
        return Response('exist')
    else:
        return Response('no')
