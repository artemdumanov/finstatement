from django.contrib.auth.hashers import BCryptPasswordHasher

class MyPBKDF2PasswordHasher(BCryptPasswordHasher):

    iterations = BCryptPasswordHasher.iterations * 100