import os

from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^', include('account_management.urls')),
    url(r'^rest/', include('services.urls')),
    url(r'^', include('basic_views.urls')),
    # url(r'^admin/', admin.site.urls),
    # url(r'^i18n/', include('django.conf.urls.i18n')),
]

# urlpatterns += i18n_patterns(
#     url(r'^', include('account_management.urls')),  # put here urls that you need to translate
# )

urlpatterns += staticfiles_urlpatterns()