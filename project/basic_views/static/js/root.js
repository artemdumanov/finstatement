/**
 * Created by artem on 10/25/16.
 */

function showSearch() {
    $('#search-button').hide('slow');
    setTimeout(function () {
        if (window.innerWidth > 769) {
            $('#search-field').focus();
            $('#search-form').show('slow');
        } else {
            $('#search-field-mobile').focus();
            $('#search-form-mobile').show('slow');
        }
    }, 430);
}

function hideSearch() {
    $('#search-form').hide('slow');
    $('#search-form-mobile').hide('slow');
    setTimeout(function () {
        $('#search-button').show('slow');
    }, 500);
}


$(window).resize(function () {
    hideSearch();

    if (window.innerWidth <= 769) {
        document.getElementById('root-div').classList.remove("margin-right-left-body");
        document.getElementById('multibar').style.display = "none"
    } else {
        document.getElementById('root-div').classList.add("margin-right-left-body");
        document.getElementById('multibar').style.display = "flex"
    }
});

function removeOrAddClasses() {
    if (window.innerWidth <= 769) {
        document.getElementById('root-div').classList.remove("margin-right-left-body");
        document.getElementById('multibar').style.display = "none"

    } else {
        document.getElementById('root-div').classList.add("margin-right-left-body");
        document.getElementById('multibar').style.display = "flex"

    }
}

var heightBeforeHiding = [];
function hideShowStatements(id) {
    var element = document.getElementById("statements-" + id);
    var folder = document.getElementById("folder-" + id);

    if (element.style.display !== "none") {
        heightBeforeHiding[id] = folder.offsetHeight;

        $('#folder-' + id).animate({
            height: "48px"
        }, {
            complete: function () {
                element.style.display = "none"
            }
        })
    } else if (element.style.display == "none") {
        $('#folder-' + id).animate({
            height: heightBeforeHiding[id]
        }, {
            start: function () {
                element.style.display = "inline"
            }
        })
    }
}

window.onload = function () {
    removeOrAddClasses();
    $('input:radio[name="type"]').change(function () {
        if ($('input:radio[name="type"]').val() == "folder") {
            $("#statement-options").fadeToggle()
        }
    });
};




