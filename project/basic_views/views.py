import logging
from datetime import date, datetime
from random import random, choice, randrange

from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View

from django.db.models import Q
from django.utils import timezone
from django.views.decorators.csrf import csrf_protect

from models.models import Statement
from models.models import StatementSection
from models.models import GroupRow
from models.models import Row
from models.models import CashFlowMove
from models.models import Notifications

log = logging.getLogger('log')


class LandingView(View):
    @method_decorator(csrf_protect)
    def get(self, request, *args, **kwargs):
        user = request.user

        if user.is_authenticated():

            ### crutch U KNOW ###
            self.row_titles = None
            now = datetime.now()

            log.info("Visit of / %s", user)

            statement = get_object_or_404(Statement, account=user)
            log.info('Select statement id=%s; of user:%s' % (statement.statement_id, user.__str__()))

            # random values #

            # for i in range(50):
            #     row_id = randrange(1,10)
            #
            #     year = randrange(2016,2017)
            #     month = randrange(1, 13)
            #     day = randrange(1, 27)
            #     move = CashFlowMove(move_name=choice(['dsdada','asdas','asdasd','asdad']),
            #                         value=randrange(1000, 2000),
            #                         movement_date=date(year, month, day),
            #                         cash_flow=randrange(5000, 7000),
            #                         row=Row.objects.get(row_id=row_id))

            ###############

            statement_sections = StatementSection.objects.filter(statement=statement)

            income = []
            expenses = []
            assets = []
            liabilities = []

            rows_sorted = Row.objects.order_by('-creation_date')
            groups_sorted = GroupRow.objects.order_by('-creation_date')

            income_rows_no_g = rows_sorted.filter(
                Q(statement_section__statement_section="Income") & Q(group__isnull=True) & Q(
                    statement_section__statement=statement) & Q(creation_date__month=now.month))
            expenses_rows_no_g = rows_sorted.filter(
                Q(statement_section__statement_section="Expenses") & Q(group__isnull=True) & Q(
                    statement_section__statement=statement) & Q(creation_date__month=now.month))
            liabilities_rows_no_g = rows_sorted.filter(
                Q(statement_section__statement_section="Liabilities") & Q(group__isnull=True) & Q(
                    statement_section__statement=statement))
            assets_rows_no_g = rows_sorted.filter(
                Q(statement_section__statement_section="Assets") & Q(group__isnull=True) & Q(
                    statement_section__statement=statement))

            groups_income = groups_sorted.filter(
                Q(statement_section__statement_section="Income") & Q(statement_section__statement=statement) & Q(
                    last_modified__date__month=now.month))
            groups_expenses = groups_sorted.filter(
                Q(statement_section__statement_section="Expenses") & Q(statement_section__statement=statement) & Q(
                    last_modified__month=now.month))
            groups_liabilities = groups_sorted.filter(
                Q(statement_section__statement_section="Liabilities") & Q(statement_section__statement=statement))
            groups_assets = groups_sorted.filter(
                Q(statement_section__statement_section="Assets") & Q(statement_section__statement=statement))

            income_total = 0
            expenses_total = 0
            assets_total = 0
            liabilities_total = 0

            income_counter = 0
            expenses_counter = 0
            assets_counter = 0
            liabilities_counter = 0

            def format_section (section, groups, rows_n_g, total, counter):
                for group in groups:
                    g = [group.group_row_name, 0]
                    rs = rows_sorted.filter(Q(group=group))

                    result = []
                    for r in rs:
                        g[1] += r.row_value
                        total += r.row_value
                        result.append([r.row_name, r.row_value])

                    g.append(result)
                    section.append(g)

                    counter += 1

                for row in rows_n_g:
                    section.append([row.row_name, row.row_value])
                    total += row.row_value
                    counter += 1

                return [total, counter]

            income_data = format_section(income, groups_income, income_rows_no_g, income_total, income_counter)
            expenses_data = format_section(expenses, groups_expenses, expenses_rows_no_g, expenses_total, expenses_counter)
            assets_data = format_section(assets, groups_assets, assets_rows_no_g, assets_total, assets_counter)
            liabilities_data = format_section(liabilities, groups_liabilities, liabilities_rows_no_g, liabilities_total, liabilities_counter)

            cash_flow_moves = CashFlowMove.objects.filter(row__statement_section__statement=statement)

            log.info('Select cash_flow_moves; statement id=%s; user:%s; %s' % (
                statement.statement_id, user.__str__(), cash_flow_moves))

            dataTable = []

            for move in cash_flow_moves:
                row = data_row.create(move)
                log.info('Create dataTable row; statement:%s; user:%s; row %s' % (statement.statement_id, user, row))
                dataTable.insert(dataTable.__len__(), row)

            if not dataTable:
                dataTable.append(["-", "-", "-", "-", "0", "0", ""])

            log.info(
                'Create dataTable; statement:%s; user:%s; dataTable %s' % (statement.statement_id, user, dataTable))

            section = request.GET.get('section')
            group = request.GET.get('group')
            row = request.GET.get('row')

            if section is not None:
                section = section.title()
                line_chart_data = CashFlowMove.objects.filter(
                    Q(row__statement_section__statement_section=section) and Q(
                        row__group__statement_section__statement_section=section))

            sections = StatementSection.objects.filter(statement=statement)
            groups = GroupRow.objects.filter(
                Q(statement_section__statement=statement) & Q(statement_section__statement_section=section))

            log.info('Select section of user:%s; statement %s; sections: %s' % (
                user, statement.statement_id, sections.values_list()))
            log.info('Select groups of user:%s; statement %s; sections: %s; groups: %s' % (
                user, statement.statement_id, sections.values_list(), groups.values_list()))

            rows = None

            if group is not None:
                rows = Row.objects.filter(
                    Q(statement_section__statement=statement) &
                    Q(statement_section__statement_section=section) &
                    Q(group__group_row_name=group.title())).distinct()
                log.info('Select rows GROUP %s of user:%s; statement %s; sections: %s; group:%s; rows: %s'
                         % (group, user, statement.statement_id, sections.values_list(), groups.values_list(),
                            rows.values_list()))

            rows_no_group = Row.objects.filter(
                Q(statement_section__statement=statement) &
                Q(statement_section__statement_section=section) &
                Q(group__row__isnull=True)).distinct()
            log.info('Select rows NO GROUP of user:%s; statement %s; sections: %s; rows: %s' % (
                user, statement.statement_id, sections.values_list(), rows_no_group.values_list()))

            notifications = Notifications.objects.filter(Q(account=user) & ~Q(open_date=None))
            log.info('Get notifications; user:%s; notifications: %s' % (user, notifications.values_list()))

            json = '{' + 'dataTable:' + dataTable.__str__() + '}'

            line_chart_data = []

            moves = CashFlowMove.objects.filter(Q(row__statement_section__statement=statement) |
                                                Q(row__group__statement_section__statement=statement)).order_by(
                '-movement_date')
            log.info('Select moves group/nogroup order by -date; user:%s; statement:%s; moves: %s' % (
                user, statement.statement_id, moves.values_list()))
            if (section is None) and (group is None) and (row is None):
                # add group tooltip
                self.row_titles = [{'type': 'date', 'label': "Date"},
                                   {'type': 'number', 'label': "Income"},
                                   {'type': 'number', 'label': "Expenses"},
                                   {'type': 'number', 'label': "Assets"},
                                   {'type': 'number', 'label': "Liabilities"},
                                   {'type': 'number', 'label': "Cash"}]

                max_dates = []
                income_max = moves.filter(Q(row__statement_section__statement_section="Income")).first()
                income_max_g = moves.filter(Q(row__group__statement_section__statement_section="Income")).first()
                if (income_max is not None) and (income_max_g is not None):
                    income_max_date_g = income_max_g.movement_date
                    income_max_date = income_max.movement_date
                    if income_max_date_g > income_max_date:
                        max_dates.append(income_max_date_g)
                    else:
                        max_dates.append(income_max_date)
                elif (income_max_g is not None) and (income_max is None):
                    max_dates.append(income_max_g.movement_date)
                elif (income_max_g is None) and (income_max is not None):
                    max_dates.append(income_max.movement_date)

                expenses_max_g = moves.filter(Q(row__statement_section__statement_section="Expenses")).first()
                expenses_max = moves.filter(Q(row__group__statement_section__statement_section="Expenses")).first()
                if (expenses_max is not None) and (expenses_max_g is not None):
                    expenses_max_date_g = expenses_max_g.movement_date
                    expenses_max_date = expenses_max.movement_date
                    if expenses_max_date_g > expenses_max_date:
                        max_dates.append(expenses_max_date_g)
                    else:
                        max_dates.append(expenses_max_date)
                elif (expenses_max_g is not None) and (expenses_max is None):
                    max_dates.append(expenses_max_g.movement_date)
                elif (expenses_max_g is None) and (expenses_max is not None):
                    max_dates.append(expenses_max.movement_date)

                liabilities_max_g = moves.filter(Q(row__statement_section__statement_section="Assets")).first()
                liabilities_max = moves.filter(Q(row__group__statement_section__statement_section="Assets")).first()
                if (liabilities_max is not None) and (liabilities_max_g is not None):
                    liabilities_max_date_g = liabilities_max_g.movement_date
                    liabilities_max_date = liabilities_max.movement_date
                    if liabilities_max_date_g > liabilities_max_date:
                        max_dates.append(liabilities_max_date_g)
                    else:
                        max_dates.append(liabilities_max_date)
                elif (liabilities_max_g is not None) and (liabilities_max is None):
                    max_dates.append(liabilities_max_g.movement_date)
                elif (liabilities_max_g is None) and (liabilities_max is not None):
                    max_dates.append(liabilities_max.movement_date)

                assets_max_g = moves.filter(Q(row__statement_section__statement_section="Liabilities")).first()
                assets_max = moves.filter(Q(row__group__statement_section__statement_section="Liabilities")).first()
                if (assets_max is not None) and (assets_max_g is not None):
                    assets_max_date_g = assets_max_g.movement_date
                    assets_max_date = assets_max.movement_date
                    if assets_max_date_g > assets_max_date:
                        max_dates.append(assets_max_date_g)
                    else:
                        max_dates.append(assets_max_date)
                elif (assets_max_g is not None) and (assets_max is None):
                    max_dates.append(assets_max_g.movement_date)
                elif (assets_max_g is None) and (assets_max is not None):
                    max_dates.append(assets_max.movement_date)

                for move in moves:
                    movement_date = move.movement_date
                    year = movement_date.year
                    month = movement_date.month
                    day = movement_date.day

                    if move.row.group is not None:
                        if move.row.group.statement_section.statement_section == 'Income':
                            line_chart_data.append(
                                [[year, month, day], move.value, 'undefined', 'undefined', 'undefined', move.cash_flow,
                                 move.move_id])
                        elif move.row.group.statement_section.statement_section == 'Expenses':
                            line_chart_data.append(
                                [[year, month, day], 'undefined', move.value, 'undefined', 'undefined', move.cash_flow,
                                 move.move_id])
                        elif move.row.group.statement_section.statement_section == 'Assets':
                            line_chart_data.append(
                                [[year, month, day], 'undefined', 'undefined', move.value, 'undefined', move.cash_flow,
                                 move.move_id])
                        elif move.row.group.statement_section.statement_section == 'Liabilities':
                            line_chart_data.append(
                                [[year, month, day], 'undefined', 'undefined', 'undefined', move.value, move.cash_flow,
                                 move.move_id])
                    else:
                        if move.row.statement_section.statement_section == 'Income':
                            line_chart_data.append(
                                [[year, month, day], move.value, 'undefined', 'undefined', 'undefined', move.cash_flow,
                                 move.move_id])
                        elif move.row.statement_section.statement_section == 'Expenses':
                            line_chart_data.append(
                                [[year, month, day], 'undefined', move.value, 'undefined', 'undefined', move.cash_flow,
                                 move.move_id])
                        elif move.row.statement_section.statement_section == 'Assets':
                            line_chart_data.append(
                                [[year, month, day], 'undefined', 'undefined', move.value, 'undefined', move.cash_flow,
                                 move.move_id])
                        elif move.row.statement_section.statement_section == 'Liabilities':
                            line_chart_data.append(
                                [[year, month, day], 'undefined', 'undefined', 'undefined', move.value, move.cash_flow,
                                 move.move_id])

                for line in line_chart_data:
                    for line_two in line_chart_data:
                        if (line[6] != line_two[6]) and (line[0] == line_two[0]):

                            if (type(line[1]) == int) and (type(line_two[1]) == int):
                                line[1] += line_two[1]
                                line_two[1] = 'undefined'

                            elif (type(line[2]) == int) and (type(line_two[2]) == int):
                                line[2] += line_two[2]
                                line_two[2] = 'undefined'

                            elif (type(line[3]) == int) and (type(line_two[3]) == int):
                                line[3] += line_two[3]
                                line_two[3] = 'undefined'

                            elif (type(line[4]) == int) and (type(line_two[4]) == int):
                                line[4] += line_two[4]
                                line_two[4] = 'undefined'

                log.info('Select all line chart data of user:%s; statement %s; data: %s' % (
                    user, statement.statement_id, line_chart_data))

                last_asset_line_chart = None
                last_liability_line_chart = None

                flagA = False
                flagL = False
                for line in line_chart_data:
                    if (type(line[3]) == int) and not flagA:
                        last_asset_line_chart = line
                        flagA = True

                    if (type(line[4]) == int) and not flagL:
                        last_liability_line_chart = line
                        flagL = True

                    line.remove(line[6])

                year = now.year
                month = now.month
                day = now.day
                today = [year, month, day]

                if last_asset_line_chart is not None:
                    last_asset_line_chart = last_asset_line_chart[:]
                    last_asset_line_chart[0] = today
                    last_asset_line_chart[5] = 'undefined'
                    line_chart_data.insert(0, last_asset_line_chart)

                if last_liability_line_chart is not None:
                    last_liability_line_chart = last_liability_line_chart[:]
                    last_liability_line_chart[0] = today
                    last_liability_line_chart[5] = 'undefined'
                    line_chart_data.insert(0, last_liability_line_chart)

                if line_chart_data is []:
                    line_chart_data.insert(0, [
                        today,
                        'undefined', 'undefined', 'undefined', 'undefined', moves.first().cash_flow])


                log.info('Add to line chart liabilities and assets edge of user:%s; statement %s; data: %s' % (
                    user, statement.statement_id, line_chart_data))

            if (section is not None) and (group is None) and (row is None):
                groups_query = groups.filter(Q(statement_section__statement_section=section))
                rows_no_group_query = rows_no_group.filter(Q(statement_section__statement_section=section))

                self.row_titles = [{'type': 'date', 'label': "Date"}]

                titles_g = []
                titles_r = []
                groups_sections = []
                rows_sections = []

                for group in groups_query:
                    self.row_titles.append({'type': 'number', 'label': '\'' + group.group_row_name + '\''})
                    groups_sections.append(moves.filter(Q(row__group__group_row_name=group.group_row_name)))
                    titles_g.append(group.group_row_name)
                for row in rows_no_group_query:
                    self.row_titles.append({'type': 'number', 'label': '\'' + row.row_name + '\''})
                    rows_sections.append(moves.filter(Q(row__row_name=row.row_name)))
                    titles_r.append(row.row_name)

                if titles_g is not None:
                    i = 0
                    for g in titles_g:
                        mvs = groups_sections[0].filter(Q(row__group__group_row_name=g))

                        for mv in mvs:
                            date = mv.movement_date
                            format_date = [date.year, date.month, date.day]

                            result = []
                            result.append(format_date)

                            for index in range(i):
                                result.append('undefined')

                            result.append(mv.value)

                            cash_flow_position = titles_g.__len__() + titles_r.__len__()

                            line_chart_data.append(result)

                        i += 1

                        # for line in line_chart_data:
                        #     print(line)


                if titles_r is not None:
                    i = 0
                    for r in titles_r:
                        mvs = rows_sections[0].filter(Q(row__row_name=r))

                        for mv in mvs:
                            date = mv.movement_date
                            format_date = [date.year, date.month, date.day]

                            result = []
                            result.append(format_date)

                            for index in range(titles_g.__len__()):
                                result.append('undefined')

                            for index in range(i):
                                result.append('undefined')

                            result.append(mv.value)

                            cash_flow_position = titles_g.__len__() + titles_r.__len__()

                            line_chart_data.append(result)

                        i += 1

                        # for line in line_chart_data:
                        #     print(line)

            currency = statement.currency
            currency = '<span class="currency">'+ currency +'</span>'

            kwargs = {
                'currency': currency,
                'statement': statement,
                'income': income,
                'expenses': expenses,
                'assets': assets,
                'liabilities': liabilities,
                'income_total': income_data[0],
                'expenses_total': expenses_data[0],
                'assets_total': assets_data[0],
                'liabilities_total': liabilities_data[0],
                'income_counter': range(5-income_data[1]),
                'expenses_counter': range(5-expenses_data[1]),
                'assets_counter': range(5-assets_data[1]),
                'liabilities_counter': range(5-liabilities_data[1]),
                'json': json,
                'moves': dataTable,
                'notifications': notifications,
                'sections': sections,
                'groups': groups,
                'rows': rows,
                'rows_no_group': rows_no_group,
                'lineChartData': line_chart_data
            }

            if self.row_titles:
                kwargs["row_titles"] = self.row_titles

            return render(request, "root.html", kwargs)
        else:
            log.info("New visit of /)")

            return render(request, "landing.html")


class data_row:
    def create(move):
        name = move.row.row_name
        value = move.value.__str__()
        cash_flow = move.cash_flow.__str__()
        id = move.move_id.__str__()
        movement_date = move.movement_date.year.__str__() + '/' + move.movement_date.month.__str__() + '/' + move.movement_date.day.__str__()

        section = None
        group = None
        if move.row.group is None:
            section = move.row.statement_section.statement_section
        else:
            section = move.row.group.statement_section.statement_section
            group = move.row.group.group_row_name

        row = [name, group, section, movement_date, value, cash_flow, id]

        return row


class AddRow(View):
    @method_decorator(csrf_protect)
    def post(self, request):
        pass
