import os

from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from basic_views import views

urlpatterns = [
    url(r'^$', views.LandingView.as_view()),
]