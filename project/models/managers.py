from django.db import models
from django.contrib.auth.base_user import BaseUserManager

isreq = ' is required'


class AccountManager(BaseUserManager):
    def create_user(self, first_name, last_name, birthday_date, sex, country, city, email,
                    mobile_number, registration_date, expired_date, account_type, password):

        fields = [first_name, last_name, birthday_date, sex, country, email,
                  mobile_number, registration_date, expired_date, account_type, password]

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            birthday_date=birthday_date,
            sex=sex,
            country=country,
            city=city,
            mobile_number=mobile_number,
            registration_date=registration_date,
            expired_date=expired_date,
            account_type=account_type,
        )

        user.set_password(password)
        user.save()
        return user