# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractBaseUser
from django.db import models

from models.managers import AccountManager


class Account(AbstractBaseUser):
    REQUIRED_FIELDS = ['first_name', 'last_name', 'registration_date', 'account_type']

    account_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    birthday_date = models.DateField(blank=True, null=True)
    sex = models.TextField(blank=True, null=True)  # This field type is a guess.
    country = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(unique=True, max_length=320)
    mobile_number = models.CharField(unique=True, max_length=45, blank=True, null=True)
    registration_date = models.DateTimeField()
    expired_date = models.DateField(blank=True, null=False)
    account_type = models.TextField()  # This field type is a guess.
    last_login = models.DateTimeField()
    email_confirmed = models.NullBooleanField()
    pass_tutorial = models.NullBooleanField()

    objects = AccountManager

    USERNAME_FIELD = 'email'


    class Meta:
        managed = False
        db_table = 'account'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class CashFlowMove(models.Model):
    move_id = models.AutoField(primary_key=True)
    move_name = models.CharField(max_length=255)
    value = models.BigIntegerField()
    description = models.CharField(max_length=255, blank=True, null=True)
    movement_date = models.DateTimeField()
    cash_flow = models.IntegerField()
    row = models.ForeignKey('Row', models.DO_NOTHING, db_column='row', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cash_flow_move'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(Account, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(unique=True, max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class GroupRow(models.Model):
    group_row_id = models.AutoField(primary_key=True)
    group_row_name = models.CharField(max_length=255)
    creation_date = models.DateTimeField()
    last_modified = models.DateTimeField(blank=True, null=True)
    total = models.BigIntegerField()
    statement_section = models.ForeignKey('StatementSection', models.DO_NOTHING, db_column='statement_section', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_row'


class Notifications(models.Model):
    notifications_id = models.BigAutoField(primary_key=True)
    message = models.TextField()
    creation_date = models.DateTimeField()
    open_date = models.DateTimeField(blank=True, null=True)
    header = models.CharField(max_length=255)
    account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account', blank=True, null=True)
    cash_flow_move = models.ForeignKey(CashFlowMove, models.DO_NOTHING, db_column='cash_flow_move', blank=True, null=True)
    timer = models.ForeignKey('Timer', models.DO_NOTHING, db_column='timer', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notifications'


class PaymentHistory(models.Model):
    payment_history = models.ForeignKey(Account, models.DO_NOTHING, primary_key=True)
    price = models.SmallIntegerField()
    begin_date = models.DateTimeField()
    expired_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'payment_history'


class Row(models.Model):
    row_id = models.AutoField(primary_key=True)
    row_name = models.CharField(max_length=255)
    row_value = models.BigIntegerField()
    creation_date = models.DateTimeField()
    last_modified = models.DateTimeField(blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    statement_section = models.ForeignKey('StatementSection', models.DO_NOTHING, db_column='statement_section', blank=True, null=True)
    group = models.ForeignKey(GroupRow, models.DO_NOTHING, db_column='group', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'row'


class Statement(models.Model):
    statement_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField()
    last_modified = models.DateTimeField(blank=True, null=True)
    free_cash_flow = models.BigIntegerField(blank=True, null=True)
    currency = models.CharField(max_length=255, blank=True, null=True)
    format_date = models.CharField(max_length=255, blank=True, null=True)
    account = models.ForeignKey(Account, models.DO_NOTHING, db_column='account', blank=True, null=True)

    def __str__(self):
        print(self.creation_date)

    class Meta:
        managed = False
        db_table = 'statement'


class StatementSection(models.Model):
    section_id = models.BigAutoField(primary_key=True)
    statement_section = models.TextField()  # This field type is a guess.
    statement = models.ForeignKey(Statement, models.DO_NOTHING, db_column='statement', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'statement_section'


class Timer(models.Model):
    time_unit = models.CharField(max_length=255)
    number = models.IntegerField()
    timer_id = models.AutoField(primary_key=True)
    row = models.ForeignKey(Row, models.DO_NOTHING, db_column='row', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'timer'
