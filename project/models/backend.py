from django.db.models import Q

from models.models import Account


class EmailOrPhoneBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            # Try to fetch the user by searching the username or email field
            user = Account.objects.get(email=username)
            if user.check_password(password):
                return user
        except Account.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            Account().set_password(password)


    def get_user(self, user_id):
        try:
            return Account.objects.get(pk=user_id)
        except Account.DoesNotExist:
            return None
