angular.module('signup', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']).controller('SignUpCtrl', function ($scope, $http) {
    $scope.validationErrors = "";

    $scope.sendPost = function () {
        if ($scope.isFormValid()) {
            data = {
                "firstName": $scope.newUser.firstName.$modelValue,
                "email": $scope.newUser.email.$modelValue,
                "password": $scope.newUser.password.$modelValue,
            }


            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie !== '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) === (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }

            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRFToken': getCookie('csrftoken'),
                }
            }

            $http.post('signup', data, config)
                .success(function (data, status, headers, config) {
                    window.location.replace('signin')
                })
                .error(function (data, status, header, config) {
                });
        }
    };

    $scope.isFormValid = function () {
        $scope.validationErrors = "";

        var notAllowedSymbolsName = ['{', '}', '_', '$', '#', '.', ',', '%', '\'', '\"', '~', ')', '(', '*', '&', ' ', '@',
            '^', '+', '-', '>', '<', '=', '№', ';', ':', '\\', '\/', ' ', '\n', '\t'];
        validateNotAllowedSymbolsAndLength($scope.newUser.firstName, "First Name", notAllowedSymbolsName, 0, 0);
        validateEmail($scope.newUser.email);
        isEmailAlreadySignedUp($scope.newUser.email)
        // isSame($scope.newUser.email, $scope.newUser.reEmail, "Emails");
        var allowedSymbolsPassword = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        validateAllowedSymbolsAndLength($scope.newUser.password, "Password", allowedSymbolsPassword, 8, 24);

        if ($scope.validationErrors == "") {
            $scope.newUser.$valid = true;
            document.getElementById('validationErrors').hidden = true;

            return true;
        } else {

            return false;
        }
    };

    function validateNotAllowedSymbolsAndLength(field, displayValue, notAllowedSymbols, minLength, maxLength) {
        var value = field.$modelValue;

        if (value == undefined) {
            value = '';
        }

        if (value !== '') {
            checkLength(value, displayValue, minLength, maxLength)

            var keepGoing = true;
            angular.forEach(value, function (symbol) {
                if (keepGoing) {
                    angular.forEach(notAllowedSymbols, function (notAllowedSymbol) {
                        if (notAllowedSymbol == symbol) {
                            document.getElementById('validationErrors').hidden = false;
                            $scope.newUser.$valid = false;
                            $scope.validationErrors = '<b>' + displayValue + '</b>' + " has certain characters that aren't allowed.";
                            keepGoing = false;
                        }
                    })
                }
            });
        } else {
            document.getElementById('validationErrors').hidden = false;
            $scope.newUser.$valid = false;
            $scope.validationErrors = '<b>' + displayValue + '</b>' + " required"
        }
    }

    function isSame(fieldOne, fieldTwo, displayValue) {
        if (!(fieldOne.$modelValue == fieldTwo.$modelValue)) {
            document.getElementById('validationErrors').hidden = false;
            $scope.validationErrors = '<b>' + displayValue + '</b>' + " do not match.";
            $scope.newUser.$valid = false;
        }
    }

    function validateEmail(filed) {
        var regex = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i)
        if (!regex.exec(filed.$modelValue)) {
            document.getElementById('validationErrors').hidden = false;
            $scope.validationErrors = "Please enter a valid " + '<b>' + 'email' + '</b>' + " address";
            $scope.newUser.$valid = false;
        }
    }

    function isEmailAlreadySignedUp(value) {
        email = value.$modelValue;
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        };
        var data = {
            "email": email
        };
        $http.post('/rest/email', data, config)
            .success(function (data, status, headers, config) {
                if (data == 'exist') {
                    document.getElementById('validationErrors').hidden = false;
                    $scope.newUser.$valid = false;
                    $scope.validationErrors = 'There is an existing account associated with ' + '<b>' + email + '</b>';
                    keepGoing = false;
                }
                console.log(data)
            })
            .error(function (data, status, header, config) {

            });
    }

    function validateAllowedSymbolsAndLength(field, displayValue, allowedSymbols, minLength, maxLength) {
        var value = field.$modelValue;

        if (value == undefined) {
            value = '';
        }

        if (value !== '') {
            if (!isContainsNumber(value)) {
                document.getElementById('validationErrors').hidden = false;
                $scope.validationErrors = '<b>' + displayValue + '</b>' + "  must contain at least one digit";
                $scope.newUser.$valid = false;
            }
            if (!(/^[a-zA-Z0-9_-]+$/i.test(value))) {
                document.getElementById('validationErrors').hidden = false;
                $scope.validationErrors = '<b>' + displayValue + '</b>' + "  has certain characters that aren't allowed";
                $scope.newUser.$valid = false;
            }

            checkLength(value, displayValue, minLength, maxLength)
        } else {
            document.getElementById('validationErrors').hidden = false;
            $scope.validationErrors = '<b>' + displayValue + '</b>' + " is required";
            $scope.newUser.$valid = false;
        }
    }

    function checkLength(value, displayValue, min, max) {
        if (!(min <= 0 || max <= 0)) {
            if (value.length < min || value.length > max) {
                document.getElementById('validationErrors').hidden = false;
                $scope.validationErrors = '<b>' + displayValue + '</b>' + " length must be at least " + min + " and " +
                    "no more than " + max;
                $scope.newUser.$valid = false;
            }
        }
    }

    function isContainsNumber(value) {
        var numbers = '0123456789';
        var isNumber = [];
        angular.forEach(numbers, function (number) {
            isNumber[isNumber.length] = value.indexOf(number) >= 0;
        });

        var flag = false;
        angular.forEach(isNumber, function (val) {
            if (val === true) {
                flag = true;
            }
        });

        return flag;
    }
});