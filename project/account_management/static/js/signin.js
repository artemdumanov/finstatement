angular.module('signin', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'dumanovValidation', 'dumanovBasic']).controller('SignInCtrl', function ($scope, $http, $location, Validator, Basic) {

    $scope.sendPost = function () {
        if ($scope.isFormValid()) {
            data = {
                "email": $scope.form.email.$modelValue,
                "password": $scope.form.password.$modelValue
            };

            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRFToken': Basic.getCookie('csrftoken')
                }
            };

            $http.post('signin', data, config)
                .success(function (data, status, headers, config) {
                })
                .error(function (data, status, header, config) {
                });
        }
    };

    $scope.isFormValid = function () {
        var form = $scope.form;
        $scope.emailError = '';
        $scope.passwordError = '';

        Validator.setForm(form);
        $scope.emailError = Validator.email(form.email.$modelValue);
        $scope.passwordError = Validator.nullValue(form.password.$modelValue, 'Password');

        var $valid = Basic.isNullOrUndefinedOrEmpty($scope.emailError) && Basic.isNullOrUndefinedOrEmpty($scope.passwordError);

        $scope.form.$valid = $valid;

        return $valid
    };
});