import json

from django.contrib.auth import login, authenticate, logout
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render, redirect

from django import views
from django.utils import timezone

from models.models import Account
from models.models import Statement
from models.models import StatementSection


class SignUp(views.View):
    def get(self, request, *args, **kwargs):
        return render(request, 'signup.html')

    @method_decorator(csrf_protect)
    def post(self, request, *args, **kwargs):
        post_dict = dict(request.POST)

        account = Account()

        for element in post_dict:

            json_post_dict = json.loads(element)

            for value in json_post_dict:

                if value == 'firstName':
                    account.name = json_post_dict[value]
                if value == 'email':
                    account.email = json_post_dict[value]
                if value == 'password':
                    account.set_password(json_post_dict[value])

        account.account_type = 'trial'
        account.registration_date = timezone.now()
        account.save()

        print(account)

        return HttpResponse('/signin')


class SignIn(views.View):
    def get(self, request, *args, **kwargs):
        user = request.user

        if user.is_authenticated:
            return redirect("/")
        else:
            return render(request, "signin.html", *args, **kwargs)

    @method_decorator(csrf_protect)
    def post(self, request):
        # json_post_dict = json.loads(dict(request.POST)[0])

        post_dict = dict(request.POST)

        print(post_dict)
        print(post_dict['email'])
        print(post_dict['password'])

        user = authenticate(username=post_dict['email'][0], password=post_dict['password'][0])

        if user is not None:
            print('NOT NONE')

            if user.last_login is None:
                statement = Statement.objects.create(creation_date=timezone.now(), account=user)
                a = StatementSection.objects.create(statement_section="Income", statement=statement)
                b = StatementSection.objects.create(statement_section="Expenses", statement=statement)
                c = StatementSection.objects.create(statement_section="Assets", statement=statement)
                d = StatementSection.objects.create(statement_section="Liabilities", statement=statement)

            login(request, user)
            return HttpResponseRedirect('/')
        else:
            print("NONE")
            return render(request, 'signin.html', {'errors': '* Invalid initials'})

class LogOut(views.View):
    def get(self, request):
        logout(request)
        return redirect("/")