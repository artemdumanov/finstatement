from django import template
from django.template.defaultfilters import stringfilter, urlize
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter(name='urlizedot')
def urlizedot(text):
    return urlize(text).replace('.', '%972%')

