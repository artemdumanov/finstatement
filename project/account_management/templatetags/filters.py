from django.utils.encoding import iri_to_uri

from account_management.templates import register
from django.template.defaultfilters import linebreaksbr, urlize

@register.filter(name='urlizedot')
def urlizedot(value):
    return value.replace('.', '%972%').replace('@', '%973%')

def deurlizadot(value):
    return value.replace('%972%', '.').replace('%973%', '@')