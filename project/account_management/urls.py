from django.conf.urls import url

from account_management import views

urlpatterns =[
    url(r'^signup$', views.SignUp.as_view()),
    url(r'^signin$', views.SignIn.as_view()),
    url(r'^logout$', views.LogOut.as_view())
]