angular.module('dumanovBasic', []).service('Basic', function () {

    /**
     * Return true if null or undefined
     **/
    this.isNullOrUndefined = function (value) {
        return value == undefined || value == null;
    };

    /**
     * Return true if null or undefined or empty
     **/
    this.isNullOrUndefinedOrEmpty = function (value) {
        return value == undefined || value == null || value == '';
    };

    /**
     * Set '' if value == null | undefined
     **/
    this.setEmptyToNullOrUndefined = function (value) {

        if (this.isNullOrUndefined(value)) {
            return '';
        } else {
            return value;
        }
    };

    /**
     * Return cookie with name cookie
     * **/
    this.getCookie = function (name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    };

    /**
     * Remove css class from element
     * */
    this.removeClass = function (element, classToRemove) {
        element.classList.remove(classToRemove)
    };
    /**
     * Add css class to element
     * */
    this.addClass = function (element, classToRemove) {
        element.classList.add(classToRemove)
    };

    /**
     * Console output
     * */
    this.c = function (value) {
        console.log(value)
    };
});