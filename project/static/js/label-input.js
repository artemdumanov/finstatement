/**
 * Created by artem on 11/23/16.
 */

function LabelInput(statementSection, allowedCells) {

    $(document).ready(function () {
        $("div#" + statementSection + " input[type=text]").each(function () {
            var id = $(this).attr('id');
            $("div#" + statementSection + ' .change input.value[type=text]#' + id).val($("div#" + statementSection + ' .change label#' + id).text());
            $("div#" + statementSection + ' .change label#' + id).text(numberWithCommas($("div#" + statementSection + ' .change label#' + id).text()));
            setTotal(statementSection)
        });
    });

    function childOf(c, p) {
        while ((c = c.parentNode) && c !== p);
        return !!c
    }

    $("html").click(function (event) {
        // alert(document.querySelectorAll('div#'+statementSection)[0])
        if (childOf(event.target, document.querySelectorAll('div#' + statementSection)[0])) hideOrShowLabelOrInput(event.target)
    });


    var lastClicked;
    var input;
    var label;

    function hideOrShowLabelOrInput(target) {
        if (target.id !== undefined && target.id !== null && target.id !== '') {
            if (allowedCells.indexOf(target.id) < 0) {
                try {
                    target.id = lastClicked.id
                } catch (e) {

                }
            }

            input = $("div#" + statementSection + " .change  input.value[type=text]#" + target.id);
            label = $("div#" + statementSection + " .change  label#" + target.id);

            if (target.tagName.toLowerCase() == 'input') {
                label.innerHTML = input.value;
            } else if (target.tagName.toLowerCase() == 'label') {
                input.val(removeNumberWithCommas(label.text()))

                label.addClass('display-none');
                input.removeClass('display-none');

                input.focus();
                setTotal(statementSection)
            } else {
                var inputLastClicked = $("div#" + statementSection + " .change  input.value[type=text]#" + lastClicked.id)
                var value = $("div#" + statementSection + " .change  input.value[type=text]#" + lastClicked.id).val()
                if (new RegExp('^[0-9]+$', 'igm').test(value)) {
                    var inputValue = $("div#" + statementSection + " .change  input.value[type=text]#" + lastClicked.id).val();

                    inputValue = numberWithCommas(inputValue)

                    $("div#" + statementSection + " label#" + lastClicked.id).text(inputValue);

                } else {
                    setInputLastClickedLabel()
                }
                if (value == '') {
                    setInputLastClickedLabel()
                }

                if (value == '0') {
                    setInputLastClickedLabel()
                }

                function setInputLastClickedLabel() {
                    var label = $("div#" + statementSection + " .change  label#" + lastClicked.id);
                    label.text(label.text());
                    var input = $("div#" + statementSection + " .change  input.value[type=text]#" + lastClicked.id)
                    input.val(label.text())
                }

                $("div#" + statementSection + " .change  input.value[type=text]#" + lastClicked.id).addClass('display-none');
                $("div#" + statementSection + " .change  label#" + lastClicked.id).removeClass('display-none');
                setTotal(statementSection)

            }
            lastClicked = target;
        }
    }


    //$('div .total > div > span > b').text('1111111111111111')
    function setTotal(statementSection) {
        var fields = allowedCells;

        var sum = 0;
        for (var i = 0; i < allowedCells.length; i++) {
            var cell = $("div#" + statementSection + ' .change  input.value[type=text]#' + allowedCells[i]).val()
            cell = removeNumberWithCommas(cell)
            cell = parseInt(cell)

            sum += cell;
        }

        sum = numberWithCommas(sum.toString())
        $('b#' + statementSection + '-total').text(sum);
    }

    function findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }
}

function isDescendant(parent, child) {
    var node = child.parentNode;
    while (node != null) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}

var currencyXML = $.parseXML('')
var statementSection;
function setModalName(modalType, statementSection) {
    this.statementSection = statementSection;

    h4 = $('div#' + modalType + '-modal > div > div> div > h4')

    if (modalType == 'add') {
        h4.html("Add new <b>" + statementSection + "</b>")
    }
}
//
// currencyXML = new DOMParser().parse (currencyXML,"text/xml");
// console.log(currencyXML)

//path
$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "../static/currencies.json",
        dataType: "json",
        success: function (json) {

            for (var key in json) {
                if (json.hasOwnProperty(key)) {
                    var value = json[key]

                    setCodeOrSymbol(value)
                }
            }
        }
    });

});

function setCodeOrSymbol(value) {
    if (value["symbol_native"] == "$") {
        $('select#currency').html($('select#currency').html() + "<option value='" + value['code'] + "'>" + value['name'] + " " + value['symbol'] + '</option>')
    } else {
        $('select#currency').html($('select#currency').html() + "<option value='" + value['code'] + "'>" + value['name'] + " " + value['symbol_native'] + '</option>')
    }
}

function addIncome() {
    var form = 'form#add-form'

    var inputs = ['name', 'value', 'currency', 'date', 'rhythm']

    inputs.forEach(function (e) {
        ($(form + '> div > div > [name=' + e + ']').val())
    })

    // $('div#income > div#footer').html($('div#income > div#footer').html() + '<div class="statement-row">' +
    //     '<div class=\"checkbox-cell\">' +
    //         '<input type=\"checkbox\" id=\"investments2-checkbox\" name=\"investments2-checkbox\">' +
    //     '</div>' +
    //     '<span class=\"currency padding-statement border-element padding-left-0\">Investments 2</span>' +
    //     '<div class=\"pull-right padding-statement flex border-element\">' +
    //         '<span class=\"currency\">$</span>' +
    //         '<label id=\"investments2\" class=\"zero-margin margin-right-4\">2000</label>' +
    //         '<input id=\"investments2\" name=\"investments2\" type=\"text\" class=\"display-none cash-cell form-control value\">' +
    //     '</div>' +
    //     '</div>')
}

function validateNumber(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;

    if (key == '8') {
        return;
    }

    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function deleteRows(statementSection) {
    $('div#' + statementSection + ' > div#footer > div.statement-row > div.checkbox-cell > input[type=checkbox]').css('display', 'inline');
    $('div#' + statementSection + '-delete-section').css('display', 'inline');
    $('span#' + statementSection + '-menu').css('display', 'none');
}

function cancelDeleteRows(statementSection) {
    $('div#' + statementSection + ' > div#footer > div.statement-row > div.checkbox-cell > input[type=checkbox]').css('display', 'none');
    $('div#' + statementSection + '-delete-section').css('display', 'none');
    $('span#' + statementSection + '-menu').css('display', 'inherit');
}

function c(t) {
    console.log(t)
}

function a(v) {
    alert(v)
}

function numberWithCommas(value) {
    value = value.replace(/\s/g, '')

    value = parseInt(value);

    var parts = value.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function removeNumberWithCommas(value) {
    value = value.replace(/\s/g, '')

    return value.replace(/\,/g, '')
}

function addRow(form, section) {

    var rowData = {"section": section};
    var formArray = form.serializeArray();

    if (section == 'Income' || section == 'Expenses') {
        $.each(formArray, function (i, value) {
            if (i == 0) {
                rowData.name = value.value;
            } else if (i == 1) {
                rowData.value = value.value;
            } else if (i == 2) {
                rowData.date = value.value;
            } else if (i == 3) {
                rowData.group = value.value;
            } else if (i == 4) {
                rowData.recurring = value.value;
            }
        });
        alert(JSON.stringify(rowData))
    }
    if (section == 'Assets') {
        $.each(formArray, function (i, value) {
            if (i == 0) {
                rowData.name = value.value;
            } else if (i == 1) {
                rowData.value = value.value;
            } else if (i == 2) {
                rowData.date = value.value;
            } else if (i == 3) {
                rowData.group = value.value;
            } else if (i == 4) {
                rowData.income = value.value;
            } else if (i == 5) {
                rowData.recurring = value.value;
            }
        });
        alert(JSON.stringify(rowData))
    }
    if (section == 'Liabilities') {
        $.each(formArray, function (i, value) {
            if (i == 0) {
                rowData.name = value.value;
            } else if (i == 1) {
                rowData.value = value.value;
            } else if (i == 2) {
                rowData.date = value.value;
            } else if (i == 3) {
                rowData.group = value.value;
            } else if (i == 4) {
                rowData.expenses = value.value;
            } else if (i == 5) {
                rowData.recurring = value.value;
            }
        });
        alert(JSON.stringify(rowData))
    }
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'somewhere', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
    xhr.onload = function () {
        console.log(this.responseText);
    };
    xhr.send(rowData);
}

