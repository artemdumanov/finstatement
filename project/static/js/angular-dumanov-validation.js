/**
 * function setForm() is required
 * */
angular.module('dumanovValidation', ['dumanovBasic']).service('Validator', function (Basic) {

    /**
     * Required function
     * Define the form, for which operations will be applied
     * **/
    this.setForm = function (form) {
        this.form = form
    };

    /**
     * Check form to none, return form
     * **/
    function checkForm() {
        if (Basic.isNullOrUndefined(this.form)) {
            new Error('Form is null or undefined')
        } else {
            return this.form;
        }
    }

    /**
     * Validate email.
     * @param value - email string
     *
     * return errors or errorTrace
     * **/
    this.email = function (value) {
        var errors = '';

        value = Basic.setEmptyToNullOrUndefined(value);

        if (value != '') {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value)) {
                errors = 'Please enter a valid ' + '<b>' + 'email' + '</b>' + ' address';
                this.form.$valid = false;
            }
        } else {
            errors = '<b>' + 'Email' + '</b>' + ' is required';
            this.form.$valid = false;
        }

        return displayErrorsOrErrorTrace(errors);
    };

    /**
     * Check value with name to regex
     * **/
    this.string = function (value, name, regex, error) {
        var errors = '';

        // if (!regex.test(value)) {
        if (!(value.match(regex))) {
            errors = error;
            this.form.$valid = false;
        }

        return displayErrorsOrErrorTrace(errors);
    };

    /**
     * Check password value with min and max length
     * **/
    this.password = function (value) {
        value = Basic.setEmptyToNullOrUndefined(value);

        var nullLengthErrors = this.nullValue(value, 'Password');
        if (!Basic.isNullOrUndefinedOrEmpty(nullLengthErrors)) {
            this.form.$valid = false;
            return displayErrorsOrErrorTrace(nullLengthErrors)
        }

        var lengthErrors = this.length(value, 'Password', 8, 24);
        if (!Basic.isNullOrUndefinedOrEmpty(lengthErrors)) {
            this.form.$valid = false;
            return displayErrorsOrErrorTrace(lengthErrors);
        }

        // Add not permitted characters
        var patternErrors = this.string(value, 'Password',
            /^(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9@#$%^&*_-]+$/i,
            'Please enter a valid <b>password</b>');
        if (!Basic.isNullOrUndefinedOrEmpty(patternErrors)) {
            this.form.$valid = false;
            return displayErrorsOrErrorTrace(patternErrors);
        }
    };

    /**
     * Check is value with name
     * **/
    this.nullValue = function (value, name) {
        if (Basic.isNullOrUndefinedOrEmpty(value)) {
            this.form.$valid = false;
            return '<b>' + name + '</b>' + " is required"
        } else {
            return null;
        }
    };

    /**
     * Check is value length more then min and less then max
     * **/
    this.length = function (value, name, min, max) {
        var length = value.length;
        if (!(length >= min && length <= max)) {
            this.form.$valid = false;
            return '<b>' + name + '</b>' + " length must be at least " + min +
                " and no more than " + max;
        } else {
            return null;
        }
    };

    /**
     * Set value which will be displayed instead default
     * **/
    this.setErrorTrace = function (errorTrace) {
        this.errorTrace = errorTrace;
        return this.errorTrace;
    };

    function displayErrorsOrErrorTrace(errors) {
        if (!Basic.isNullOrUndefinedOrEmpty(errors)) {
            if (!Basic.isNullOrUndefinedOrEmpty(this.errorTrace)) {
                this.form.$valid = false;
                return this.errorTrace;
            } else {
                return errors;
            }
        }
    }
});